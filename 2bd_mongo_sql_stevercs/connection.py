import pymongo
import mysql.connector

def connMsql():
    try:
        mysqlcon=mysql.connector.connect(
        host='localhost',
        user='root',
        password='',
        database='mydb'
    )
        print("Connecté à MySQL")
        return mysqlcon
    except mysql.connector.Error as error:
        print("Erreur lors de la connexion à MySQL :", error)
        return None

def connexion_mongodb():
    try:
        # Remplacez "url_mongodb" par l'URL de connexion à votre base de données MongoDB
        client = pymongo.MongoClient("mongodb+srv://dabakhsalledu:Amadou@cluster0.ycia1gn.mongodb.net/?retryWrites=true&w=majority")
        # Remplacez "nom_de_la_base_de_donnees" par le nom de votre base de données MongoDB
        db = client.mydb
        return db  # Retourne la connexion à la base de données
    except pymongo.errors.ConnectionFailure as e:
        print(f"Erreur de connexion à MongoDB : {e}")
        return None  # En cas d'échec de connexion, renvoie None
