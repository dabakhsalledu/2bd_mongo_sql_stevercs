
from connection import connexion_mongodb,connMsql
import time
mongo = connexion_mongodb()
mongo_collection=mongo['Clients']
Mysqlcon=connMsql()
cusor=Mysqlcon.cursor()

def create_client(ID_Client,nom, prenom, email):
    if Mysqlcon:
        # Requête d'insertion
        insert_query = f"INSERT INTO Clients (ID_Client,Nom, Prenom, Email) VALUES ({ID_Client},'{nom}', '{prenom}', '{email}')"
        # Exécution de la requête
        cusor.execute(insert_query)
        # Commit des changements
        Mysqlcon.commit()
        print(f"Client {nom} {prenom} ajouté avec succès.")
    else:
        print("Erreur de connexion à MySQL.")


def read_clients():
    if Mysqlcon:
        # Requête de sélection
        select_query = "SELECT * FROM Clients"
        # Exécution de la requête
        cusor.execute(select_query)
        # Récupération des résultats
        clients = cusor.fetchall()
        cusor.close()
        clients_as_dicts = [{'ID_Client': client[0], 'Nom': client[1], 'Prenom': client[2], 'Email': client[3]} for client in clients]
        return clients_as_dicts
        # Affichage des résultats
        # for row in results:
        #     print(row)
    else:
        print("Erreur de connexion à MySQL.")


def update_client(id_client, nom, prenom, email):
    if Mysqlcon:
        # Requête de mise à jour
        update_query = f"UPDATE Clients SET Nom = '{nom}', Prenom = '{prenom}', Email = '{email}' WHERE ID_Client = {id_client}"
        # Exécution de la requête
        cusor.execute(update_query)
        # Commit des changements
        Mysqlcon.commit()
        print(f"Client {nom} {prenom} mis à jour avec succès.")
    else:
        print("Erreur de connexion à MySQL.")

def delete_client(id_client):
    if Mysqlcon:
        # Requête de suppression
        delete_query = f"DELETE FROM Clients WHERE ID_Client = {id_client}"
        # Exécution de la requête
        cusor.execute(delete_query)
        # Commit des changements
        Mysqlcon.commit()
        print(f"Client avec ID {id_client} supprimé avec succès.")
    else:
        print("Erreur de connexion à MySQL.")



def read_mysql_changes():
        cusor.execute("SELECT ml.ID, ml.ID_Client, ml.operation, c.Nom, c.Prenom, c.Email FROM modification_log ml JOIN clients c ON ml.ID_Client = c.ID_Client WHERE ml.synced = 0")
        changes = cusor.fetchall()
        print(changes)
        return changes

def sync_mysql_to_mongodb():
        changes = read_mysql_changes()
        # print(changes)
        for change in changes:
            operation = change[2]  # Assurez-vous que le champ 'operation' est à la position appropriée dans le tuple
            
            if operation == 'INSERT':
                mongo_collection.insert_one({
                    "ID_Client": change[1],
                    "Nom": change[3],
                    "Prenom": change[4],
                    "Email": change[5]
                })
                print("insert in mongodb")
            elif operation == 'UPDATE':
                mongo_collection.update_one(
                    {"ID_Client": change[1]},
                    {"$set": {
                        "Nom": change[3],
                        "Prenom": change[4],
                        "Email": change[5]
                    }}
                )
                print("update in mongodb")
            elif operation == 'DELETE':
                mongo_collection.delete_one({"ID_Client": change[1]})
                print("delete in mongodb")
            else:
             print("pas de modification actuellement")
            # Marquer la modification comme synchronisée
            cusor.execute("UPDATE modification_log SET synced = 1 WHERE ID = %s", (change[0],))  # Assurez-vous que l'ID est à la position appropriée dans le tuple
            Mysqlcon.commit()
            
        # Fermeture du curseur après avoir terminé toutes les opérations
        # print("pas d'opération pour le moment")
        cusor.close()

# while True:
#     Mysqlcon = connMsql()  # Établir la connexion MySQL à chaque itération
#     cursor = Mysqlcon.cursor()
   
#     time.sleep(5)  





# Fonction de synchronisation avec MongoDB
# def sync_mysql_to_mongodb():
#     mysql_clients = read_mysql_clients()
#     for client in mysql_clients:
#         existing_client = mongo_collection.find_one({'ID_Client': client['ID_Client']})
#         if existing_client:
#             mongo_collection.replace_one({'_id': existing_client['_id']}, client)
#         else:
#             mongo_collection.insert_one(client)


# create_client(1,"Kondjo","henri","henrikondjo@gmail.com")
        





# Fonction de synchronisation avec MongoDB
# def sync_mysql_to_mongodb():
#     mysql_clients = read_clients()
#     for client in mysql_clients:
#         existing_client = collection.find_one({'ID_Client': client['ID_Client']})
#         if existing_client:
#             collection.replace_one({'_id': existing_client['_id']}, client)
#             print("remplacement dans mongodb")
#         else:
#             collection.insert_one(client)
#             print("insertion dans mongodb")

# create_client(1,"Dupont", "Jean", "jean.dupont@example.com")
# # update_mysql_client(1, "Dupont", "Jeanne", "jeanne.dupont@example.com")
# # delete_mysql_client(2)
# # res=read_clients()
# # print(res)
# # Synchronisation avec MongoDB après chaque modification
# sync_mysql_to_mongodb()
# while True:
