from connection import connexion_mongodb,connMsql
from flask import Flask, request,jsonify
import time
from fonctionmongodb import ajouter_client,mettre_a_jour_client,lire_clients,supprimer_client
from fonctionmsql import create_client,read_clients,update_client,delete_client,read_mysql_changes,sync_mysql_to_mongodb
from flask_cors import CORS  # Importez CORS depuis flask_cors
app = Flask(__name__)
CORS(app)  # Activez CORS pour votre application Flask

app = Flask(__name__)
mongo = connexion_mongodb()
mongo_collection=mongo['Clients']

@app.route('/clients', methods=['POST'])
def add_client():
    data = request.get_json()
    create_client(data['ID_Client'], data['Nom'], data['Prenom'], data['Email'])
    return jsonify({'message': 'Client ajouté avec succès'}), 201

@app.route('/clients', methods=['GET'])
def get_clients():
    clients = read_clients()
    return jsonify(clients), 200

@app.route('/clients/<int:id_client>', methods=['PUT'])
def edit_client(id_client):
    data = request.get_json()
    update_client(id_client, data['Nom'], data['Prenom'], data['Email'])
    return jsonify({'message': f'Client avec ID {id_client} mis à jour'}), 200

@app.route('/clients/<int:id_client>', methods=['DELETE'])
def remove_client(id_client):
    delete_client(id_client)
    return jsonify({'message': f'Client avec ID {id_client} supprimé'}), 200

@app.route('/sync', methods=['GET'])
def sync_mysql_mongodb():
    Mysqlcon = connMsql()
    cursor = Mysqlcon.cursor()
    def read_mysql_changes():
        cursor.execute("SELECT ml.ID, ml.ID_Client, ml.operation, c.Nom, c.Prenom, c.Email FROM modification_loog ml JOIN clients c ON ml.ID_Client = c.ID_Client WHERE ml.synced = 0")
        changes = cursor.fetchall()
        print(changes)
        return changes

    def sync_mysql_to_mongodb():
     
        changes = read_mysql_changes()
        for change in changes:
            operation = change[2]

            if operation == 'INSERT':
                mongo_collection.insert_one({
                    "ID_Client": change[1],
                    "Nom": change[3],
                    "Prenom": change[4],
                    "Email": change[5]
                })
                
            elif operation == 'UPDATE':
                mongo_collection.update_one(
                    {"ID_Client": change[1]},
                    {"$set": {
                        "Nom": change[3],
                        "Prenom": change[4],
                        "Email": change[5]
                    }}
                )

            elif operation == 'DELETE':
                mongo_collection.delete_one({"ID_Client": change[1]})
            else:
                print("pas de modification actuellement")

            cursor.execute("UPDATE modification_loog SET synced = 1 WHERE ID = %s", (change[0],))
            Mysqlcon.commit()

        cursor.close()
        return "Synchronization completed!"

    result = sync_mysql_to_mongodb()
    return jsonify({'message': result})


if __name__ == '__main__':
    app.run(debug=True)