
import mysql.connector

def create_triggers(table_name, id_column):
    # Établir la connexion à la base de données MySQL
    mysql_connection = mysql.connector.connect(
        host='localhost',
        user='root',
        password='',
        database='mydb'
    )

    # Commandes SQL pour créer les déclencheurs
    sql_triggers = f"""
    CREATE TRIGGER {table_name.lower()}_insert_trigger AFTER INSERT ON {table_name}
    FOR EACH ROW
    BEGIN
        INSERT INTO modification_loog (ID, operation) VALUES (NEW.{id_column}, 'INSERT');
    END;

    CREATE TRIGGER {table_name.lower()}_update_trigger AFTER UPDATE ON {table_name}
    FOR EACH ROW
    BEGIN
        INSERT INTO modification_loog (ID, operation) VALUES (NEW.{id_column}, 'UPDATE');
    END;

    CREATE TRIGGER {table_name.lower()}_delete_trigger AFTER DELETE ON {table_name}
    FOR EACH ROW
    BEGIN
        INSERT INTO modification_loog (ID, operation) VALUES (OLD.{id_column}, 'DELETE');
    END;
    """

    # Exécution des commandes SQL pour créer les déclencheurs
    try:
        cursor = mysql_connection.cursor()
        cursor.execute(sql_triggers, multi=True)
        print(f"Déclencheurs pour la table {table_name} créés avec succès !")
    except mysql.connector.Error as error:
        print(f"Erreur lors de la création des déclencheurs pour la table {table_name} :", error)

    # Fermer la connexion
    mysql_connection.close()

# Exemple d'utilisation pour la table Clients
create_triggers('clients', 'ID_Client')
