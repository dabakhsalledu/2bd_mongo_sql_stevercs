from connection import connMsql,connexion_mongodb
import mysql.connector
Mysqlcon=connMsql()
cusor=Mysqlcon.cursor()

mongo = connexion_mongodb()

def tableMysql():
    if Mysqlcon:
        # Requêtes de création de tables
        clients = '''
        CREATE TABLE Clients (
            ID_Client INT  PRIMARY KEY,
            Nom VARCHAR(255),
            Prenom VARCHAR(255),
            Email VARCHAR(255)
        )
        '''      
    # Exécution des requêtes pour créer les tables
    try:
        cusor.execute(clients)
        # Commit des changements
        Mysqlcon.commit()
        print("Tables créées avec succès.")
    except mysql.connector.Error as error:
        print("Erreur lors de la création des tables :", error)
    # Fermeture du curseur et de la connexion à MySQL
    cusor.close()
    Mysqlcon.close()
# tableMysql()

def create_mongodb_collection():
    if mongo is not None:
        # Création de la collection
        mongo.create_collection("Clients")
        # Ajout des champs à la collection
        mongo.Clients.create_index([("ID_Client", 1)], unique=True)
        mongo.Clients.create_index([("Nom", 1)])
        mongo.Clients.create_index([("Prenom", 1)])
        mongo.Clients.create_index([("Email", 1)])
        print("Collection créée avec succès dans MongoDB.")
    else:
        print("Erreur de connexion à MongoDB.")

create_mongodb_collection()