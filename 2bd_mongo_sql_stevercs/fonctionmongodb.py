from connection import connexion_mongodb
from pymongo.errors import DuplicateKeyError
mongo = connexion_mongodb()
collection=mongo['Clients']

# Fonction pour ajouter un client
def ajouter_client(id_client, nom, prenom, email):
    try:
        collection.insert_one({
            "ID_Client": id_client,
            "Nom": nom,
            "Prenom": prenom,
            "Email": email
        })
        print("Client ajouté avec succès.")
    except DuplicateKeyError:
        print("ID_Client existe déjà, veuillez choisir un autre ID.")

# Fonction pour lire tous les clients
def lire_clients():
    clients = collection.find()
    for client in clients:
        print(client)

# Fonction pour mettre à jour un client
def mettre_a_jour_client(id_client, nouveaux_champs):
    collection.update_one({"ID_Client": id_client}, {"$set": nouveaux_champs})
    print("Client mis à jour avec succès.")

# mettre_a_jour_client(1, {"Nom": "Doe", "Prenom": "Jane"})
# Fonction pour supprimer un client
def supprimer_client(id_client):
    collection.delete_one({"ID_Client": id_client})
    print("Client supprimé avec succès.")


# ajouter_client(1,"kondjo","henri","henrikondjo@gmail.com")